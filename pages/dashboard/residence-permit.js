import { withNamespaces } from 'i18n';
import { DashboardLayout, QrCodeItem } from 'components';
import { getQrCode, awaitStatus } from 'utils/sockets';
import { randomString } from 'utils';
import Router from 'next/router';

class ResidencePermit extends React.Component {
  state = {
    residencePermit: {
      request: {},
      offer: {},
    },
  };

  async componentDidMount() {
    const identifier = randomString(5);
    const { residencePermit } = this.state;

    try {
      const requestQrCode = await getQrCode({
        socketName: 'residence-permit/sso',
        query: {
          identifier,
        },
      });
      residencePermit.request.source = requestQrCode;
      this.setState({ residencePermit });

      await awaitStatus({
        socketName: 'residence-permit/sso',
        query: {
          identifier,
        },
      });
      residencePermit.request.success = true;
      this.setState({ residencePermit });

      const offerQrCode = await getQrCode({
        socketName: 'residence-permit/offer',
        query: {
          identifier,
        },
      });

      residencePermit.offer.source = offerQrCode;
      this.setState({ residencePermit });

      await awaitStatus({
        socketName: 'residence-permit/offer',
        query: {
          identifier,
        },
      });
      residencePermit.offer.success = true;
      this.setState({ residencePermit });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { t } = this.props;
    const { residencePermit } = this.state;

    if (!this.props.profile.did) {
      Router.push('/');
      return {};
    }

    return (
      <DashboardLayout>
        {(residencePermit.request.source || residencePermit.offer.source) && (
          <div className="row center-xs">
            <div className="col-xs-12 col-sm-6 ">
              <QrCodeItem
                note={`1. ${t(
                  'dashboard.tabs.residence-permit.instructions.requirement'
                )}:`}
                {...residencePermit.request}
              />
            </div>
            <div className="col-xs-12 col-sm-6 ">
              <QrCodeItem
                note={`2. ${t(
                  'dashboard.tabs.residence-permit.instructions.offer'
                )}:`}
                {...residencePermit.offer}
              />
            </div>
          </div>
        )}
      </DashboardLayout>
    );
  }
}

ResidencePermit = withNamespaces()(ResidencePermit);

export default ResidencePermit;
