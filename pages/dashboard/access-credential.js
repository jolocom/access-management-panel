import React from 'react';
import { connect } from 'react-redux';
import { withNamespaces } from 'i18n';
import { DashboardLayout, QrCodeItem, AccessGraph } from 'components';
import { getQrCode, awaitStatus } from 'utils/sockets';
import Router from 'next/router';

class AccessCredential extends React.Component {
  state = {
    credentialOffers: {
      accessKey: {},
    },
  };

  handleSubmitForm = async(doors) => {
    const { credentialOffers } = this.state;

    try {
      // qrCode, socket, identifier
      const { qrCode: idCardQrCode, socket, identifier } = await getQrCode(
        'receive',
        {
          credentialType: 'accessKey',
          data: JSON.stringify({token: doors.toString()}),
        }
      );

      credentialOffers.accessKey.source = idCardQrCode;
      this.setState({ credentialOffers });

      await awaitStatus({ socket, identifier });
      credentialOffers.accessKey.success = true;

      this.setState({ credentialOffers });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { t } = this.props;
    const { credentialOffers } = this.state;

    if (!this.props.profile.did) {
      Router.push('/');
      return {};
    }

    return (
      <DashboardLayout>
        <div className="u-container">
          {credentialOffers.accessKey.source ? (
            <div className="row center-xs">
              <div className="col-xs-12 col-xs-6">
                <QrCodeItem
                  {...credentialOffers.accessKey}
                />
              </div>
            </div>
          ) : (
            <div style={{ marginBottom: 120 }}>
              <AccessGraph
                onSelectionFinished={this.handleSubmitForm}
                graph={this.props.graph}
                style={{
                  width: 800,
                  height: 600
                }}
              />
            </div>
          )}
        </div>
      </DashboardLayout>
    );
  }
}

AccessCredential = withNamespaces()(AccessCredential);

AccessCredential = connect(
  ({
    user: { profile },
    ui: { globalError, countries },
    data: { graph },
  }) => ({
    profile,
    globalError,
    countries,
    graph,
  }),
)(AccessCredential);

export default AccessCredential;
