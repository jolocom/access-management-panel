import * as TYPE from 'actions';

const INITIAL_STATE = {
  globalError: '',
  countries: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPE.UI_SET_GLOBAL_ERROR:
      return {
        ...state,
        globalError: action.payload,
      };
    case TYPE.UI_SET_COUNTRIES:
      return {
        ...state,
        countries: action.payload,
      };

    default:
      return state;
  }
};
