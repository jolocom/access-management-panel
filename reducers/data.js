import * as TYPE from 'actions';

const INITIAL_STATE = {
  graph: undefined,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPE.DATA_SET_GRAPH:
            return {
                ...state,
                graph: action.payload,
            };
        default:
            return state;
    }
};
