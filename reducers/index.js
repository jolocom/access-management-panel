import { combineReducers } from 'redux';

import user from './user';
import ui from './ui';
import data from './data';

const rootReducer = combineReducers({
  user,
  ui,
  data,
});

export default rootReducer;
