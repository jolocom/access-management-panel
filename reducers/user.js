import * as TYPE from 'actions';

const INITIAL_STATE = {
  profile: {
    did: '',
    email: '',
    givenName: '',
    familyName: '',
    residence: '',
    postalCode: '',
  },
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPE.USER_SET_PROFILE:
      return {
        ...state,
        profile: action.payload,
      };

    case TYPE.USER_UPDATE_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...action.payload,
        },
      };

    case TYPE.USER_CLEAR:
      return INITIAL_STATE;

    default:
      return state;
  }
};
