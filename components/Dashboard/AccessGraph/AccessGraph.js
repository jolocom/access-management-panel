import {
  Button,
} from '@acpaas-ui/react-components';

import ClickableGraph from 'clickable-graph-component'

class AccessGraph extends React.Component {
  constructor(props) {
    super(props)
    this.state = { selection: [] }
  }

  render() {
    return (
      <div>
        <ClickableGraph
          graph={this.props.graph}
          onLinkClicked={
            link => {
              if (this.state.selection.includes(link.id)) {
                this.setState({ selection: this.state.selection.filter(l => l !== link.id) })
              } else {
                this.setState({ selection: [...this.state.selection, link.id] })
              }
            }
          }
          onNodeClicked={node => console.log(node.id)}
          style={{
            graph: this.props.style,
            node: {
              radius: 10,
              color: 'red'
            },
            link: {
              width: 10,
              color: 'blue',
              maxLen: 30
            }
          }}
        />
        <Button onClick={() => this.props.onSelectionFinished(this.state.selection)}>submit</Button>
        <p>{this.state.selection.toString()}</p>
      </div>
    );
  }
};

export default AccessGraph;
