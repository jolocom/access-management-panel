import * as TYPE from 'actions';

export const setCountries = countries => ({
  type: TYPE.UI_SET_COUNTRIES,
  payload: countries,
});
