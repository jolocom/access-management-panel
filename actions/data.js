import * as TYPE from 'actions';

export const setGraph = graph => ({
  type: TYPE.DATA_SET_GRAPH,
  payload: graph,
});
